# Let's build a Mesh!

Welcom to Project Phoenix's mesh table. Feel free to follow this guide to easily
get connected. Once you're online you can start playing around with the typology, 
build a distributed application, or play some games!

#### **If you would like to use your personal machine to get connected** 

- Apart from the utility required for easy network configuration (Eg. batctl).
  The changes made to your wireless interface _should not_ persist after
  rebooting your machine. However, we do not take any responsibility for
  whatever happens to your equipment, connecting is at **YOUR OWN RISK!**. 

- At this time, we have not tested Microsoft Windows, however that doesn't mean
  you can't attempt to get onto the mesh with your NT based OS. Let one of the
  Project Phoenix Volunteers know if you do succeeed in joining!

- Questions and concerns can be addressed directly by the Project Phoenix
  volunteers, please don't be afraid to ask for help if you get stuck!!!

With that, let's get started!

#### Log onto a laptop:
- - -  

  - Username: **azbi** 
  - Password: **ProjPX**

  **NOTE**:
  - The computer should automattically log in for you on startup.
  - Usernames may differ, you can verify your's by opening a terminal and typing
  
  ```
  $ whoami
  
  azbi

  ```

  - The password also will be used for `sudo`

#### Set up the system
- - -

  This should be easy, most likely the software necessary to connect to the mesh
  is already installed. However, we suggest you still follow these incoming
  steps to ensure you got everything on your laptop. 
  
  **Step 1**. Connect to the wifi.

  **Step 2**. Patch your system! Open a terminal and follow along.

  ```
  $ sudo apt-get update
  ```

  **Step 3**. Install the necessary software:

  ```
  sudo apt-get install batctl
  ```

#### Connect to the mesh!
- - -

  We've set up a few mesh devices. Follow along to get onto the mesh:

  **Step 1**. Disable NetworkManager.service
  
  NetworkManager.service configures the system's network interfaces, for us to
  work on the interfaces directly, we must disable the service. 
  
  ```
  $ sudo systemctl stop NetworkManager.service
  ```

#### Make a note of the current interfacese. 
- - - 
  
  To view all available network interfaces on the system, use the following
  command.

  ```
  $ sudo ip link 
  ```

  The output from this command will show you the currently configured
  interfaces. We'll be focusing on `wlan0`.
  
  Example:

  ```
  ... truncated output ...

  3: wlan0: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether b4:6d:83:59:45:0c brd ff:ff:ff:ff:ff:ff

  ... truncated output ...
  ```

#### Set up the wireless NIC for mesh networking.
- - -

  This step allows us to enable ad-hoc mode on the interface. Ad-hoc mode allows
  the wireless NIC to talk to other ad-hoc devices on the same IBSS (Mesh SSID).

  ```
  $ sudo iw wlo1 del
  $ sudo phy phy0 interface add wlan0 type ibss
  $ sudo ip link set up mtu 1532 dev wlan0
  ```
  
  Set the wireless NIC to talk over the mesh wireless network
 
  ```
  $ sudo iw dev wlan0 ibss join mesh 2412 HT20 fixed-freq
  ```

  Now, let's enable the batman-adv kernel module so that we can set up the
  protocol. 

  ```
  $ sudo modprobe batman-adv
  ```

  Our mesh software needs to know where to send/listen to traffic. 

  ```
  $ sudo batctl if add wlan0
  ```
  
#### Set up bat0 
- - -

  Now let's set up an interface for handling traffic. This is done since wlan0
  is now in use in the system and cannot understand B.A.T.M.A.N's protocols. The
  Interface we're going to set up can understand and transmit/receive our
  future traffic. 

  ```
  $ sudo ip link set up dev bat0
  ```

#### Check to see that everything is working!
- - -

  Let's see if everything is working.

  ```
  $ sudo batctl n
  ```

  You should see a list of other mesh nodes currently connected. 
  
  **Example:**

  ```
  $ batctl n

  [B.A.T.M.A.N. adv 2017.4, MainIF/MAC: wlan0/b4:6d:83:59:45:0c
  (bat0/ba:8f:fd:28:dd:bb BATMAN_IV)]
  IF             Neighbor              last-seen
         wlan0   b4:6d:83:59:36:ee    0.084s
  ```

#### Let's get an address.
- - -

  The wireless interface is now set up to talk on the B.A.T.M.A.N mesh, however
  we still need to get an IP address so that we can access network resources.

  After issuing a DHCP request, use the `ip addr` command to confirm you're able
  to participate in TCP/IP communications. 

  ```
  $ sudo dhclient -4
  
  $ ip addr

  5: bat0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state
  UNKNOWN group default qlen 1000
      link/ether ba:8f:fd:28:dd:bb brd ff:ff:ff:ff:ff:ff
   --->    inet 10.27.0.27/24 brd 10.27.0.255 scope global bat0
          valid_lft forever preferred_lft forever
  ```

#### Profit!
- - -

  If everything was set up correctly you should now be able to talk to other
  devices on the mesh network! 

--------------------------------------------------------------------------------
### Additional Resources
Interested in mesh networking, check out these websites and make something

- https://www.open-mesh.org/projects/batman-adv/wiki/Doc-overview
- https://en.wikipedia.org/wiki/Mesh_networking
- http://www.meshnetworks.com/

## Meshing in Phoenix!

We're a group of volunteers working toward building our own mesh in Phoenix, you
can find out more by visiting our site:

```
https://azblockchain.org/project-phoenix/
```
