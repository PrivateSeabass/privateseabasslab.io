---
layout: post
title: "Six Degree Objectives"
date: 2019-02-6T23:03:00-07:00
author: PrivateSeabass
summary: >
  The course objectives of UAT and the projects that meet those objectives.

categories: project
thumbnail: fa-uat
tags:
 - UAT
 - Degree
 - Objectives
 - Projects
---

# Introduction
The University of Advancing Technology has six degree objects for a Network Security major.  
Here are the objectives and the assignments that provide evidence for completion.  

## First Objective
Create a network infrastructure design communications document that includes identified hardware components, connections to outside world, identified physical layer connectivity (media) and addressing, including optional and security components in design.

##### Cosmonaut Company Network Diagram
[Packet Tracer Network](/assets/networkDiagrams/SpaceRoute.pkt)  
[Image Version of Packet Tracer Network](/assets/networkDiagrams/SpacialNetwork.jpg)
This is a network Diagram of a hypothetical space technology company.  
The reason it is in packet tracer is my capability to configure the whole network.    
The degree of detail is down to the attachments to devices on the network.  
For other relevant documents that were made before the Packet Tracer Network:  
[Network Diagram](/assets/networkDiagrams/SpacialNetwork.jpg)  
[Write-up for Reasoning behind design](/assets/networkDiagrams/SpacialNetworkWriteUp.pdf)

#### SIP Project 
[Project Phoenix Gitlab](https://gitlab.com/projectphx/Project_Mystery-Castle/config/automatedSetup)
This project was automates setup of mesh networking devices.
The playbooks automate configuration of the /etc/network/interfaces file and can be used on any Debian Linux based OS.
Network devices are configured for startup and the device is configured to communicate on a network using [B.A.T.M.A.N. adv.](https://www.open-mesh.org/projects/open-mesh/wiki).


## Second Objective
Install, configure and test security hardware and software tools with supporting documentation such as port scanners, vulnerability detection systems, intrusion detection systems, firewalls, system hardening, anti-virus tools, patch management, auditing and assessment.

#### SIP Project (Uplink Playbook)
[Project Phoenix Gitlab](https://gitlab.com/projectphx/Project_Mystery-Castle/config/automatedSetup)
The Uplink of the project requires IPtable rules to be set to allow for traffic to leave the device and communicate with the internet.
This uses Ansible to set rules, save rules, and configure on startup.

#### Easy Start Linux
[Easy Start Linux Repository](https://gitlab.com/PrivateSeabass/easy-start-linux)  
This was made in response to two things.  
1. Linux installs can break a lot when you don't know what you are doing.... Linux also gets reinstalled a lot. This makes it easier to reinstall and/or transition to a new distro.
2. People are afraid of starting Linux, sometimes. So I just give them this, due to how much easier it makes our lives. (also a useful teaching tool)
3. Take your choice of strictly bash or Ansible based installation


## Third Objective
Construct, implement and document a script or a program to automate a security-related process or other tasks such as installation, administration, management, mapping resources, logon scripts, patch management, updates, auditing, analysis and assessment.

#### Easy Start Linux
[Easy Start Linux Repository](https://gitlab.com/PrivateSeabass/easy-start-linux)  
This was made in response to two things.  
1. Linux installs can break a lot when you don't know what you are doing.... Linux also gets reinstalled a lot. This makes it easier to reinstall and/or transition to a new distro.
2. People are afraid of starting Linux, sometimes. So I just give them this, due to how much easier it makes our lives. (also a useful teaching tool)

#### Ansible SSH-key Distribution Playbook
[The Post](project/2019/02/11/Ansible-sshkey-distribution/)  
[A Different version of the playbook from the Post](/assets/playbooks/deliverSSH-key.yml)  
Some research was done on how to distribute public SSH-keys through Ansible.  
Read some content on people being bothered by Ansible being incapable of password-less ssh before key distribution.  
This was made as a result....  
Later found a module to do it. (read post for link)  
It is still good for teaching people Ansible, though.  

## Fourth Objective
Create a policy or procedure that addresses events such as: a disaster recovery plan, a business continuity plan, an incident response policy, an acceptable usage document, an information security policy, a physical security policy, assessments or troubleshooting procedures.  

#### Business Continuity and Disaster Recovery Documentation for DigiKnight Technologies (In Development)
A project created by a team to ensure DigiKnight recovers quickly with minimal losses to disasters such as tornadoes, earthquakes, fires.
The document discusses factors the company has or needs to take into consideration, such as insurance, and who to contact for recovery.
This document is also intended to assess where possible risks/acceptable vulnerabilities are located for the company.

#### Technically Private
[Technically Private's Presentation Slides](/assets/presentations/powerpoints/technicallyPrivate.pdf)  
I've done multiple presentations at the Phoenix Linux User's Group.  
Most are security oriented.  
This one details more advanced software and physical security practices.  
As of writing this, a video has not been posted, but it will be in [here](https://www.youtube.com/user/fone626/videos?pbjreload=10) when it gets posted.  
(Fair Warning: Link goes to Brian Cluff's YouTube page.)  

## Fifth Objective
Develop a research report or implementation plan concerning legal and ethical best practices and mandated requirements that pertain to information security.

#### Offensive or Defensive?
[Active Defense Research Paper](/assets/Papers/offensiveOrDefensive.pdf)
At one point (maybe still happening), there was a bill purposed that would allow companies to "hack-back".  
In the paper, alternative active defense measures are considered and implications around "hacking-back" is discussed.

#### Ansible SSH-key Distribution Playbook
[The Post](/project/2019/02/11/ansible-sshkey-distribution/)  
[A Different version of the playbook from the Post](/assets/playbooks/deliverSSH-key.yml)  
Some research was done on how to distribute public SSH-keys through Ansible.  
Read some content on people being bothered by Ansible being incapable of password-less ssh before key distribution.  
This was made as a result....  
Later found a module to do it. (read post for link)  
It is still good for teaching people Ansible, though.  

## Sixth Objective
Research, document, test and evaluate several current industry information security based threats, risks, malicious activities, covert methodology, encryption technologies, mitigation techniques or unconventional tactics to prevent loss of sensitive information and data confidentiality, integrity and availability

#### Offensive or Defensive?
[Active Defense Research Paper](/assets/Papers/offensiveOrDefensive.pdf)
At one point (maybe still happening), there was a bill purposed that would allow companies to "hack-back".  
In the paper, alternative active defense measures are considered and implications around "hacking-back" is discussed.  

#### Technically Private  
[Private Mobile](/assets/presentations/powerpoints/technicallyPrivate.pdf)  
I've done multiple presentations at the Phoenix Linux User's Group.  
Most are security oriented.  
This one details Android and iOS mobile security and privacy practices (more than simply settings).  
As of writing this, a video has not been posted, but it will be in [here](https://www.youtube.com/user/fone626/videos?pbjreload=10) when it gets posted.  
(Fair Warning: Link goes to Brian Cluff's YouTube page.)  
