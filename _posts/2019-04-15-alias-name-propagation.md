---
layout: post
title: "Alias Name Propagation"
date: 2019-05-23T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Some people have asked "How do I hide my name?".
  Some people wonder why bother hiding a name.
  People who consider it, tend to find it stressful.
  Here are some answers.
categories: project
thumbnail: fa-aliases
tags:
 - security
 - privacy
 - alias
 - names
 - identity
---
[cardpost]: /project "blog post on payment cards"
[privacy]: privacy.com "Pay with Privacy"
[blur]: https://dnt.abine.com/#feature/payments "blur card masking"
[privacyBooks]: https://inteltechniques.com/books.html "Inteltechniques books page"
[glarb]: https://www.youtube.com/watch?v=u3CCo3X5yVA "Dennis Glarbsttentenford"
[jordanharbinger]: https://www.jordanharbinger.com/todd-herman-the-life-transforming-power-of-performing/ "Jordan Harbinger Episode"
[alterEgoBook]: https://www.amazon.com/Alter-Ego-Effect-Identities-Transform/dp/0062838636/ref=as_li_ss_tl?keywords=Todd+Herman&qid=1550624580&s=books&sr=1-1&linkCode=sl1&tag=jhsbooks-20&linkId=438928e539256747d67e894e6bb16371&language=en_US "The Alter Ego Effect"  
[privacypodcast]: https://inteltechniques.com/blog/2018/10/26/the-complete-privacy-security-podcast-episode-096/ "privacy podcast episode"
[FBIstory]: https://www.ibtimes.co.uk/fbi-directors-secret-instagram-twitter-accounts-uncovered-using-simple-trick-1614764 "FBI OSINT Story"
[inteltechniquesservices]: https://inteltechniques.com/services.html "Intel techniques services page" 
[protonmail]: protonmail.com "protonmail"
[tutanota]: tutanota.com "tutanota"
[textnow]: textnow.com "textnow"
[googleVoice]: google.com/voice "google voice"
<<<<<<< HEAD
[about]: PrivateSeabass.com/about.md "site's about page"
=======
[about]: PrivateSeaBass.com/about.md "site's about page"
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
[4]: https://4ch.net "4chan"
[8]: https://8ch.net "8chan"


This post is written for those who do not want their legal name to be the same as their common name.
The first question one might receive is "_WHY?!_", and the answer is dependent on the person.

First, the following topics are common easy answers for why people may want to obfuscate their real name.  
As it goes on, I will get into more and more extraneous circumstances for why alias usage may be desired.  

I AM NOT A LAWYER, NOR AM I YOUR LAWYER.  
You CAN go too far and break the law.
DO NOT call yourself a government official nor governmental authority (including police).
If you do, you are breaking the law.  

As a useful rule of thumb, government documents REQUIRE real information, not doing so is fraud.  
Examples: Employment forms, loan contracts, taxes.  
Anywhere that doesn't need information from nor is the government does not legally require your real information.  
Examples: Hotels, restaurants, libraries, gym memberships, Amazon.

KEEP IN MIND: Aliases take a LONG time to propagate and to obtain _some_ of the more advanced privacy benefits, people need to generally know you by another name.  
Obtaining privacy is far more difficult once you need it.
By the time one needs privacy, if no work towards maintaining it has been made, then the ability to obtain privacy *may* be too late.

If you stop needing the explanations and just want answers, skip down to the solutions section.
I know people are different; I won't hold it against you.

### Online Purchases
For the simplest of online transactions, these are some of the required transaction information. 
* Name
* Expiration date
* CVV number (3 digit thing)  
* Address

I have seen no address needed, but it most of the time is.

When a purchase is made, card information data is sent to the company and used for the transaction by the company.
One might notice that card transactions do not always show up immediately after the purchase.
Some companies employ test transactions, too.  

For all of these situations, companies need to store customer's card information until the bank approves of the purchase.
I don't think they need to keep the card information afterward, but unless someone automated the removal (which is more work and sometimes counter-intuitive for convenience), the card information is stored in the site's database.  

A user may have a card and only put it under the "billing" information, however, companies also sell that billing information and tie it to different information given in the purchase.
With an Alias, a different name is associated with the provided PII (Personally Identifiable Information), thus creating disinformation.

I will make a separate post and link it [here (not linked yet)][cardpost] once I create it.
The short version is that one's real name on the card could allow an attacker to easily find the owner and more information on the owner to commit identity fraud, if not only create an inconvenient hassle in replacing the card.

### Separation of Life
This section is more about an alter ego.  
The basis of my information on this subject is from this podcast: [163: Todd Herman | The Alter Ego Effect][jordanharbinger].  
[This is the book it references][alterEgoBook].  

The idea is that one is better at what they are doing in the moment, if they think of themselves as another person.  
Many people have many reasons to desire an alter ego, for different portions of one's life. 
To not get any of the information wrong, I would recommend using these resources for the information on the subject..  

Just as well, if you think I am interpreting or doing something wrong in my solution recommendations, send me an [e-mail][about], and we will talk about it.

### Doxing
Doxing: public release of PII 
examples:
* Birth Date
* E-mail
* Phone Number
* Social Security Number

A common place to find an alias is platforms that ask for an username (ex: Steam, Discord, Slack, etc..).
They are not the user's name (most of the time), but that is how people identify each other on these platforms.  

Here is where the concern lies:  
On sites like [4chan][4] or [8chan][8], there are many people with violently differing opinions. 
If I were to get one furious, which is not that hard to do and could easily be accidental, his response might be to investigate who I am.
He would find as much information as possible by any means (legal and otherwise) and post it somewhere clear on the internet for anyone to use.  

"Why? Who cares that my information is on the internet?
It isn't like I have anything to hide." 
(Discounting Social Security Number, I realize most people know the problem with that being public.)  

Well, with an e-mail being posted anywhere, it increases the amount of people spam that e-mail.  
With anything tied to that e-mail, now we can make more particular e-mails for the person, thus making them harder to catch.  
With the username, it is likely that you will be locatable on other platforms. (more places to spam)  
With a phone number, now you get spam calls, vishing (calls meant to steal), possibly threats (because someone was infuriated).  
With a birthday, one has a greater change to verify who you are on any platform that asks for it.  

One thing to not forget: just because this is YOUR information, does not mean this doesn't affect those who are close to you.
If someone is gathering information on you, knowing who is close to you is useful.  
If your real name is found on the internet, chances are that your spouse's name will be found. 
Maybe the spouse posts on social media more, and talks about the kids.
Kids?! Well.... Maybe those kids don't have good privacy settings, and share even more usable information to find you.

Two fascinating examples about how much information can be found, even with good privacy practices and a full name:  
Podcast Episode: [The Complete Privacy and OSINT show Episode 96][privacypodcast]  
Article Story: [FBI Director's secret Instagram and Twitter accounts uncovered using simple trick][FBIstory].

### High-Profile Individuals 
High-profile individuals may include people who work in government, celebrities, CEOs, etc...

High-profile individuals are generally well known by the general population. 
A face may not be easily recognized, however a name often is.
The possible desire for privacy comes as soon as someone sees the person's name at a hotel, bar, or any other public venue.  

Meaning: They may want less people to take photos with, to getting somewhere faster (such as a hotel room), to prevent personal information (such as phone number) from leaking to fans or journalists.
(I do not know everything they would want.)

### Victims of Stalkers and Abusive People
There are numerous types of stalkers, some may want to physically follow, some may cyber-stalk, and others will just watch people from behind corn.
There are also numerous types of abusers, some are spouses, some are relatives, and others may be workplace individuals.  
I do not know every type of situation and a threat model needs to be made.  
If the stalker is proficient in computer exploitation, there is much more that one should do than use aliases.
If this is you, I would like to direct you to [Inteltechniques services][inteltechniquesservices].
Even if you aren't being tracked by someone with proficient knowledge, those services may be desirable dependent on how much of a threat you want to protect against.  

If the desired protection is lesser, you probably want to set up more protections than just an alias. 
For individuals like these, we want to prevent them from obtaining PII (Personally Identifiable Information).
Information, such as addresses, emails, phone numbers, etc. are worth protecting under these circumstances.  
We would want to prevent abusers from obtaining any/more control.

## Solutions
##### Online Purchases
Some banks allow users to get a nickname cards.
This prevents further research on a person from being easily possible.
"But can't the research on someone still be done?"
Yes, but for most attackers it is no longer worth going after by that point.  
For a bigger threat, one who is targeting you for some reason may be capable of relating the nickname on the card to your real name with enough research.  

For this, a bank card with a nickname isn't enough.  
There are two options that I am aware of at this point: [Privacy.com][privacy] and [abine.com (blur)][blur].  
Both of the above mentioned sites provide the user with a masked card that the user can choose to apply whatever name is desired.
If I'm not mistaken, one can set up as many cards with as many aliases as is desired for both.  

For disinformation, why not make masked cards with a different full name on them?   

##### Separation of Life (Alter Ego)
In listening to the resources mentioned in the problem section for "Separation of Life", I had a thought.
Take someone you idolize on a given subject, and use their name.  
Something to be wary of is that if the person is a government official (illegal), someone you know (suspicious and could become problematic), or take an alias with the desire imitate a _real_ person (could become defamatory, possibly even accidentally).  
Keep in mind, one of the objectives is to keep people from thinking about your name as much as possible.
The more they think about it, the more likely they will get suspicious, which is not desirable for privacy (we want to keep eyes off us, not on) and for easier social situations.  

So, how about just taking the first/last name? 
Someone who also knows your idol would be far less likely to call attention to it.
Further benefits to this idea are described in Doxing.  

Depending on the method you choose, it is possible for it to be counter-intuitive to the actual effect, but if nothing else, it can help one choose a name.

##### Doxing
When registering for something online, use an alias.

Examples:
* Don't use "Seabass.Chan@gmail.com"
  * use an unrelated username like "TheAverageJoe@gmail.com".  
* Rather than "@Seabass-Chan", 
  * use "@SecretSea".  

We COULD also get into the realm of disinformation.
Disinformation can make data-brokers believe you are someone else leaving an even harder to follow trail for attackers.  

Replacement examples: 
* "Seabass" -> "Chloe".  
* "Mr. Chan" -> "Mr(s). Smith". 

Remember: If the username is going to be used as an identifier, make sure other users may not access your real name, assuming it is on the account.

##### Celebrities and Wealthy Individuals
Celebrities or wealthy people would want to use a full name alias.  
If the name on a coffee cup, for a hotel room, or anything similar, aliases may be desirable.
Most people use their legal names, so reading a fake name from the records would give you a good foundation for name deniability. 
Since people usually trust the record, they are also liable to think you just look _really similar_, and (depending on the personality) may just not say anything.

More might be needed for some people. 
Obtaining something similar to a gym card with the alias name and a photo is usually effective at deterring anyone who asks for identification.  
AGAIN: IT IS ILLEGAL TO USE AN ALIAS FOR LEGAL/GOVERNMENT RELATED DOCUMENTS.
Hotels, coffee shops, barbers are fine, but NOT ANYTHING THAT USES GOVERNMENT DOCUMENTATION!

### Victims of Stalkers and Abusive People
For these people, not only would a full name alias be useful, but they may want to make the alias look like someone else.  

A completely different, gender neutral, common, full name would be the hardest to associate. 
Not only would all the previous sections be useful, but an e-mail, phone number, social media page, and possibly more would make an alias more convincing.

There are numerous e-mail sites which will allow the creation of a new e-mail without much identifying information, like [Protonmail][protonmail] and [Tutanota][tutanota].
For burner phone numbers, there are resources like [Google Voice][googleVoice] or [Textnow][textnow]. 
Social media may require real phone numbers, making a new sim cards desirable.
Social media would make this alternative person believable.  

I am providing a much shorter version for this, as someone with this threat model needs much more information than is in going to be detailed in this post.
Here are some [books][privacyBooks] find more information.

### When Deciding What to Change
For first names, one could tell everyone that is met in a business environment an alias name and keep the family with the real name.
This is useful, as anyone who calls and asks for one name defines how the person is known and what the person will want.  

Last names have different benefits, there is no relation to someone by last name. 
Since last names are more varied, it is could make it easier or harder to specify the person that is being searched for.

There are two ideas I want to point out, as some people would think the name would not catch on.
1. Simply saying a different name on introductions is enough for most people to call someone by an alias name.
   Both "He has no reason to tell me a lie, why would you lie about your name?" and "Oh, this is what you prefer to be called" are likely first thoughts that come to people's mind, especially when initially learning someone's name.  
   For a [comical example by Dennis Regan (YouTube)][glarb].
2. If you can put a story behind the nickname, it will be much easier to prevent as much suspicion when you can explain why people may call you something entirely different.

Where it may make it harder for a person to find your information online, this does not mean one can not figure out you just changed the first name. 
Since investigators search people with the same last names (probable relatives), the changed first name is easier to find than the changed last name.
That being said, changing only the last name may be more effective for privacy, not to mention full name\*.

You may be thinking about officially changing your name (full or otherwise).
This may be an option, but it removes quite a bit of the privacy that goes with it.
Once your name is officially altered, it will be on record and anything with the new name is now directly associated to you.  
One purpose for an alias is to protect you from people attempting to steal your legal information, and legally changing a name removes that protection.
