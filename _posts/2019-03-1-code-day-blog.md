---
layout: post
title: "1 Mar. 2019: Code Day"
date: 2019-03-1T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  An attempt to remember other aspects.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
---
[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[CDdocuments]: /assets/code_day_2019.md 'Documentation from Code Day' 
[CD]: https://www.codeday.org/ 'Code Day site'

During the February 2019 [Code Day][CD], [Project Phoenix][ProjectPhoenix] set up a mesh network for students to come and learn what mesh networks are and how to use them. 
The students had clear interest, as we filled the room. 
We got [documentation][CDdocuments] setup that would allow them to set up mesh devices without needing to set configuration files. 
In this case, we had the students type in commands and setup temporary commands. 
Temporary, as in, "Screw up? Don't know how to fix it? Just restart the computer." 

This documentation only set up the client devices. 
It does not serve DHCP, let alone handle internet connections on its own.


