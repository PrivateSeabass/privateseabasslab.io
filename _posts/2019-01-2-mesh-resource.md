---
layout: post
title: "Mesh Network Resources"
date: 2019-02-3T13:23:43-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Database of links to content on mesh networks.
  Since I am working with people to develop a mesh network, here are the resources we currently have.
  For others, this is introduction into what a mesh network is, where our development has reached, examples of other mesh networks, and hopefully a generally good resource for anyone who wants to look into mesh networking and bring it to their area.

categories: resources 
thumbnail: fa-resources
tags:
  - networking 
  - mesh 
  - project 
  - phoenix
---

<<<<<<< HEAD
* Possibly usable hardware 
=======
* Possibly useable hardware 
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
  * [Ubiquiti NanoStation M5](https://store.ui.com/products/nanostation-m5)
  * [Mikrotik OmniTIK 5 PoE ac](https://mikrotik.com/product/rbomnitikpg_5hacd)	
  * [TP-Link WR842ND](https://www.amazon.com/TP-Link-N300-Wireless-Wi-Fi-Router-TL-WR841N/dp/B001FWYGJS/ref=sr_1_4?keywords=TP-Link+WR842ND&qid=1559602332&s=gateway&sr=8-4)
  * [Raspberry Pi](https://www.amazon.com/GL-iNet-GL-B1300-Gigabit-400Mbps-pre-installed/dp/B079FJKZV8/ref=sr_1_1?keywords=glb1300&qid=1559602823&s=gateway&sr=8-1)

* Quick-Start Pages
  * [Libremesh](https://libremesh.org/docs/en_quick_starting_guide.html)
  * [Althea](https://forum.altheamesh.com/t/setting-up-your-new-althea-router/159)
  * [BATMAN-adv](https://www.open-mesh.org/projects/batman-adv/wiki/Quick-start-guide)
  * [BATMAN-adv (on startup)](https://www.open-mesh.org/projects/batman-adv/wiki/Debian_batman-adv_AutoStartup)
	
* Mesh Legality
  * [EFF documents on the legality of sharing networks](https://openwireless.org/)

* Mesh Documentation
  * [Open Mesh Wiki (Linux)](https://www.open-mesh.org/projects/open-mesh/wiki)
  * [Automatic Android-basedWireless Mesh Networks (PDF)(Android)](/assets/externalPDFs/713-716-1-PB.pdf)

* Mesh Projects
  * [Project Phoenix Gitlab](https://gitlab.com/projectphx/Project_Mystery-Castle)
  * [Open-Mesh Project, Press, and Papers list](https://www.open-mesh.org/projects/open-mesh/wiki/Experience)
  * [NYC Mesh](https://www.nycmesh.net/)
  * [Sudo Room](https://sudoroom.org/wiki/Mesh)
  * [Oakland's People's Open Mesh](https://peoplesopen.net/)
  * [Personal Telco](https://personaltelco.net/wiki)
  * [Volk Fi](https://volkfi.com/) (Phone networks)
    * [Extra info](https://news.ycombinator.com/item?id=19437963)
  * [goTenna](https://gotenna.com/) 
  * [PirateBox](https://piratebox.cc/doku.php)
  * [SPAN](https://github.com/monk-dot/SPAN)

* Mesh Project chat rooms
  * [Toronto Mesh (Matrix)](https://chat.tomesh.net/#/group/+tomesh:tomesh.net)
  * [Libremesh (Matrix)](https://matrix.to/#/!xWIRAARPdhfjrGpwAZ:musaik.net?via=matrix.org&via=matrix.guifi.net&via=tomesh.net)
  * [Althea (Matrix)](https://matrix.to/#/!MKREUBPbabkbWvCRxk:matrix.org?via=matrix.org&via=t2bot.io&via=tomesh.net)
  * [Althea (Matrix)](https://matrix.to/#/!mtWPMupjiIWFGLImTm:matrix.org?via=matrix.org&via=t2bot.io&via=tomesh.net)
  * [CJDNS (Matrix)](https://matrix.to/#/!kJAwfzxqbtCfmTAixA:m.trnsz.com?via=m.trnsz.com&via=matrix.org&via=tomesh.net)
  * [Yggdrasil (Matrix)](https://matrix.to/#/!DwmKuvGvRKciqyFcxv:matrix.org?via=matrix.org&via=privacytools.io&via=tomesh.net)


* Articles
  * [A plan to rescue the Web from the Internet](https://staltz.com/a-plan-to-rescue-the-web-from-the-internet.html)
  * [Android phones are connecting without carrier networks](https://www.networkworld.com/article/2224025/android-phones-are-connecting-without-carrier-networks.html)
  * [Difference between Babel and CJDNS](https://www.reddit.com/r/darknetplan/comments/aw6gy4/difference_between_babel_and_cjdns/)
  
