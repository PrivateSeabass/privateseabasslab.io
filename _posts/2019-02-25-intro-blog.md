---
layout: post
title: "25 Feb. 2019: Intro"
date: 2019-02-25T22:10:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  This is the first post on the blog that I will be using to update whoever has interest on my findings on mesh networking.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - Start
 - update
 - project
 - phoenix
 - mesh
 - networking
---

When I first came onto the team of [Project Phoenix][ProjectPhoenix], the prototype, [Project Mystery Castle][PMC], had already been created.  
Meaning: A functional mesh up-link, gateway, and client(s) has already been created and is functional.  
These devices communicated using the [B.A.T.M.A.N. Advanced][BATMAN] protocol.  

I was informed about various [other large scale Mesh Networking projects][Mesh-Resources] that are currently happening. 
We are looking to them for guidance at the same time as finding anything about [other projects][Mesh-Resources] that exist.  
Know of any more? Please send me an email about it.  

There are about six people we can easily say are on the project team. 
[The Lead Developer][Lead] is the one who setup the prototype. 
He knows more than I do myself about how Linux operates thus understand how [B.A.T.M.A.N.][BATMAN] operates better.
I am the automated setup and update developer.
I take everything that [Lead][Lead] has documented and setup scripts to automate setup.
We both work on the documentation of [Project Mystery Castle][PMC] and realize there are currently errors or something missing. We are working finding what that is.  

This blog will mostly be dedicated to the developments of [Project Phoenix][ProjectPhoenix] and other findings that pertain to mesh networking.


[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
