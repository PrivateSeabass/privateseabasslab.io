---
layout: post
title: "26 Feb. 2019: PersonalTelco"
date: 2019-02-26T14:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  This is a post on the blog that I will be using to update whoever has interest in my findings on mesh networking and Project Phoenix.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
 - personaltelco
---

[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[PersonalTelco]: https://personaltelco.net/wiki 'Personal Telco'

Some of us who work on [Project Phoenix][ProjectPhoenix] are getting together, today. I'll update this blog or make one tomorrow detailing the updates on the project from that.  

I had recently communicated with the president of [Personal Telco][PersonalTelco], another non-profit group in Portland, Oregon. 
They promote the development of freely accessible WiFi and internet access, and are fittingly on the [EFF node page](https://www.eff.org/node/98455).  
As I had been told, they derived all their information from the [B.A.T.M.A.N. Advanced site][BATMAN]. 
This does not mean the conversation we had didn't have useful information, though.  

[Personal Telco][PersonalTelco] works primarily with a "hot-spot" model. 
There are several wireless meshes, and the devices use [B.A.T.M.A.N. Advanced][BATMAN] on [OpenWRT](https://openwrt.org/) devices. 
They also do a virtual mesh network over VPN tunnels using olsrd and openvpn. 
As the president of [Personal Telco][PersonalTelco], Russell Senior, informed me that the prime reason they don't have more meshes is due to short houses and tall trees. (Those interfere with radio waves)

His method of obtaining compatible OpenWRT firmware is from [NodeWatcher](https://wlan-si.net). 
[NodeWatcher](https://wlan-si.net) is a group from Slovenia that appears to also have large free internet initiative. 
Many pages are still under construction or currently not functional, as of writing this, but I may try to talk to them later.
*EDIT*: The [open-mesh site has instructions to set up](https://www.open-mesh.org/projects/batman-adv/wiki/Building-with-openwrt), if NodeWatcher doesn't work out.

If you are like me and didn't find what he was talking about, the open-mesh.org site has content on OpenWRT compatibility.

The documentation that Russell Senior was provided on how they setup mesh:

```
You need to install the packages

alfred, batctl and kmod-batman-adv

then add a stanza to /etc/config/network like so:

config interface 'mesh'
    option mtu '1532'
    option proto 'batadv'
    option mesh 'bat0'

and add bat0 to the interfaces on your lan network:

config interface 'lan'
    option ifname '<whatever-is-there-now> bat0'

Then your /etc/config/wireless should have add a new stanza:

config wifi-iface
    option device 'radio0'
    option ifname 'mesh0'
    option network 'mesh'
    option mode 'adhoc'
    option ssid '<your-mesh-ssid>'
    option encryption 'none'

You want to turn off dnsmasq on one of the device

Try two devices in the same room at first to make sure it works and you can ping back and forth, then try farther apart.

```

With this new information [Project Phoenix][ProjectPhoenix] may have a new method of connecting devices and/or a new method of setting up uplink nodes. We haven't gotten into it much yet, but we will see if this information effects change in our current setup.

