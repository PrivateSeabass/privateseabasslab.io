---
layout: post
title: "12 Mar. 2019: Volk-Fi & GoTenna"
date: 2019-03-24T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  A blog post on new mesh projects that I have learned about.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - mesh
 - networking
 - volkfi
 - gotenna
---
[MDB]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[got]: https://gotenna.com 'GoTenna'
[volk]: https://volkfi.com/ 'VolkFi'
[FCC]: https://FCC.gov 'Federal Communications Commissions'
[r/volk]: https://www.reddit.com/r/VolkFi/ 'volk-fi Reddit'
[RouterPost]: https://www.reddit.com/r/VolkFi/comments/b1lhev/get_more_volk_fi_routers/ 'Reddit post: Get more Volk Fi Routers?'
[PricePost]: https://www.reddit.com/r/VolkFi/comments/b3qk5e/volkfi_price/ 'VolkFi Price post'
[gotennaSpread]: https://imeshyou.gotennamesh.com/ 'map for goTenna devices'
[gotennaCon]: https://gotennamesh.com/products/mesh 'goTenna mesh'
[gotennaMil]: https://gotennapro.com/ 'goTenna pro'
[gotennaApp]: https://txtenna.com/ 'goTenna app'
[SDK]: https://gotenna.com/pages/sdk 'goTenna SDK'
[biodegrade]: https://privateseabass.gitlab.io/mesh-blog/2019/02/27/sci-fi-mesh/ 'my scifi story' 
[nuke]: https://privateseabass.gitlab.io/mesh-blog/2019/03/05/the-nuke/ 'story on meshes and nukes'
[police]: https://privateseabass.gitlab.io/mesh-blog/2019/03/08/government-possibilities/ 'Friends with the Man post'
[volkArea]: https://www.reddit.com/r/VolkFi/comments/b4jzyy/volk_fi_current_reservation_map/ 'reddit on reserved volk ones'

There are two different mesh products I have recently been made aware of, [Volk-Fi][volk] and [GoTenna][got]. Each of these use mesh protocols of SOME kind, and each of which are proprietary (as far as I can tell) in both hardware and software.

## Volk-Fi
![Image of a Volk-Fi phone](/assets/images/VolkFi.png)  

[Volk-Fi][volk] is a phone that currently says it will be released in December 2019.  
For now, it is an invite only projects (still have to pay for hardware).  

The selling idea here is circumventing cellular connections, and uses mesh protocols to reach the internet and each other.
To use the mesh instead of cellular data, users need the phone to reach a router (provided by company) that is the uplink node to the internet.  
Users have a 5GB data cap, then need to pay $1 per GB  
OR  
Users need to supply others with internet connection, through the router they supplied.  
Meaning: The router and phone are tied together somehow.  
Clarification: This is real mesh, signals will hop through phones to reach a router if needed.  
[Post on spread as of writing][volkArea]

Doing things this way not only allows for distributed, not decentralized nor centralized internet, and (debatably) free internet.  

The [Volk-Fi][volk] phone is stock Android and can otherwise be a regular phone, but prioritizes mesh.  
[A Reddit post][RouterPost] says they will be selling the routers separately at some point.  
[This other post][PricePost] says the price is ~$400.

If you want an Invite, [r/VolkFi][r/volk] seem to have inviting people.

## GoTenna
![Image of goTenna "mesh"](/assets/images/Gotenna.png)

[GoTenna][got] provides mesh network gateway hardware and software for using the mesh.  
They have created a [consumer-grade][gotennaCon], [military-grade][gotennaMil], [the app to go with them][gotennaApp], and [SDK for developers][SDK].  

The devices connect to android or iOS devices through Bluetooth.  
The goTenna devices communicate through a wireless mesh protocol.  
The [app][gotennaApp] allows users to communicate with each other through the mesh network (not the internet).  
If devices can not communicate directly, they can hop through another device that is nearer, which will relay that data further (if it can).  
This does NOT get you on to the internet.

[Current spread of devices][gotennaSpread]

One can send more than texts using this device.
It can communicate with other devices, so technically, anything that could be done on the internet could be done on this network. 
The tools just need to be made to do that.

In previous posts I have described applications for something [like][nuke] [these][police].  
Now, the question is when it will become [biodegradable][biodegrade].

## Considerations
The first thing I looked for in these software was if they were open-source. 
This was largely due to a desire to use them, or see their software and implement it in some way.
Needless to say, I was disappointed to find out both of these were proprietary.

Proprietary software, in this case is not inherently bad, though. 
These two projects have made me consider "could this be how to make these devices succeed?".  
The current problem with most mesh networks people try to put up these days is the failure to earn income. 
You can look at [my mesh database][MDB] and see numerous other mesh projects that were community driven and open-source.
Today, projects that are being released are finding some form of monetization, which is needed to keep them alive. 

Thus, something needs to change:  
One option is make proprietary code that is simply better enough to make people care.  
Another could be to make hardware and sell that as a product that comes preinstalled with the software.  
Like these two vendors, you could do both.  

The problem from choosing the proprietary software is difficulty distributing to a particular area enough to make the network desirable.  
Open-sourcing the software would allow more people to start using it in an area, but now fewer people may want to buy.  
The problem with selling preinstalled hardware is a need to work with [FCC][FCC] regulations.  
People most of the time won't make hardware, and I don't really know FCC regulations for distributing stuff, so... ¯\\\_(ツ)\_/¯  
