---
layout: post
title: "Introduction To Shodan"
date: 2018-06-19T04:52:43-07:00
author: RetroMe
summary: >
  Introduction To Shodan is a two-hour course designed to provide an overview
  of the search engine for finding devices connected to the internet. Shodan is
  a security researcher tool that works by scanning the entire internet,
  locating and parsing banners, and then returning this information to the
  user. Shodan is an excellent tool to familiarize yourself with if you do not
  have the infrastructure or tools necessary to run masscan yourself. Shodan is
  useful in the target selection phase of an operation.
categories: computers
thumbnail: fa-desktop
tags:
 - shodan
 - sigint
 - osint
 - reconaissance
 - phoenix linux users group
---

## Performance Objective

At the conclusion of the course the student will be able to:

1. Identify what Shodan is.

2. Explain how to search with Shodan.

3. Explain how to build an alternative to Shodan.

4. Explain where Shodan is often used in the hacking time line.

## Introduction

[Shodan][shodan] is a search engine for internet connected devices. It allows
you to explore the internet, see a big picture, gain a competitive advantage,
or monitor network security. The Shodan search engine is simple to use and
should be familiar to most internet users. It functions in a manner similar to
Google, DuckDuckGo, or other search engine. You can look for specific items or
search for keywords.

## How does Shodan Work

Shodan themselves have declared that they use a 'home-grown, distributed port
scanner' in order to search the internet. There exist approximately
4,294,967,296 (four billion two hundred ninety-four million nine hundred
sixty-seven thousand two hundred ninety-six) IPv4 Addresses. This pool is
32-bits in size. Scanning these addresses is an embarrassingly parallel
workload and can be easily distributed over any number of systems. The term
embarrassingly parallel simply means that little or no effort is needed to
seperate this problem into a number of parallel tasks. Password cracking and 3d
video rendering are also examples of this type of problem.

## Is Shodan Illegal?

I am not a lawyer and I am not your lawyer.

The [Computer Fraud and Abuse Act][cfaa] is a computer trespass statute that
could potentially come into play when using Shodan or Masscan within the United
States. Widespread Scanning could fall into several provisions revolving around
scanning.

1. "intentionally access a computer without authorization or exceed authorized
   access, and thereby obtain ... information from any protected computer[.]"

2. "knowingly cause the transmission of a program, information, code, or
   command, and as a result of such conduct, intentionally cause damage without
   authorization, to a protected computer[.]"

3. "intentionally access a protected computer without authorization, and as a
   result of such conduct, recklessly cause damage[.]"

4. "intentionally access a protected computer without authorization, and as a
   result of such conduct, cause damage and loss[.]"

The CFAA functions as both civil as well as criminal statute and violations can
result in criminal prosecution, fines, and prison time. Private parties harmed
by violations can sue for injunctive relief (force people to do something) or
for money.

So is Shodan or Masscan a case of intentionally accessing a protected computer
without authorization that could cause harm or loss? It depends on if a
prosecutor is interested in causing you harm. Laws like the CFAA are in place
to allow for enforcement discretion or [selective enforcement][selective].

Prosecutors hold wide latitude in deciding when, who, how, or even whether they
should prosecute for violations of this crime. If they decide that your
scanning is an affront to someone of note, you will find yourself in court.

Shodan is probably safer than Masscan as you are moving the burden of
performing the scanning from your control to someone elses.

## Why use Shodan

Not every one has the knowledge and resources to use tools like masscan to
replace Shodan. It can be a daunting task to install Elastic Search, deploy
Masscan, parse the results, upload to their server, and then search and peer
through the data. Shodan simplifies this mess by doing all of the heavy lifting
for you.

### Reconaissance

Publicly available information is the best tool for developing an attack plan.
Your initial efforts should be spent long on preperation and research. Shodan
is a good tool for discovering weaknesses or potential areas for exploitation. 

First we install dnsutils so we get access to the `host` command.

```
$ sudo pacman -S dnsutils
```

Run the `host` command on dell.com.

```
$ host dell.com
```

Run a `whois` on the IP and pipe to less.

```
$ whois 143.166.147.101|less
```

![Running the Host Command][hostgif]

The resulting CIDR: is our network range we want to scope out using Shodan.

Go to Shodan and type in `net:143.166.0.0/16` into the search.

Total Results: 912

All Results In The United States with most in Round Rock / Austin TX area.

#### Top Services:
* HTTP
* HTTPS
* SSH
* NTP
* DNS

#### Operating Systems:
* Windows 7 or 8 is 11 
* Linux 2.6.x is 2

#### Top Products:
* Microsoft IIS httpd
* Apache
* Microsoft HTTPAPI httpd
* ntpd
* OpenSS

Another interesting check might be port 27960. Go to Shodan and use
`port:27960` to see how many Quake servers are potentially online.

Other searches can include -

postal:
* `postal:85225`

city:
* `apache city:austin`

country:
* `vnc country:US`

hostname:
* `nginx hostname:'google'`

net:
* `cisco net:'IP/24'`

os:
* `https os:windows`

port:
* `port:27960`

title:
* `title:"Chandler Police Department"`

html(content):
* `html:"Chandler Police Department"`

![How a search for the Windows OS looks.][shodansearchpic]

### Honey Pots

A honey pot is a software suite or server configuration designed to appear
vulnerable but is instead intended to allow a defendor the ability to review
and watch what an attacked may be doing. The idea being that you can learn much
from an attacker who is operating at full capacity against what they believe to
be a vulnerable system. An extensive list of [honey pots][honeypots] can be
found on Github.

Many honey pots can be defeated with the use of the `cat` command. Some servers
will use generic defaults that can be added to a script for reviewing a server.
You can quickly cat a file like `/etc/passwd` and see if it matches the same as
the contents found in the honey pot configuration. If the two items match then
you can assume the system is a honey pot and disconnect immediately.

```
$ cat /etc/passwd
```

This is not ALWAYS the case but can be a safe rule of thumb and simple check.

### Business Intelligence

Created a product and need to know who is using it in an insecure manner? You
can easily run searches for your product or port and keep a running tally of
all public facing instances of your product. You could do this to your
competitor as well and potentially find companies who need your 'assistance'. 

### Target Acquisition

Shodan provides plenty of information about the makeup of the internet. As so
far as we are allowed to know. Some testing reveals that Shodan appears to
employ a list similar to the [masscan-exclude list compiled byrjmolesa][excludescan]. 
This exclusion list includes networks for Government Agencies, FBI Honeypots,
Military Targets, and more.

Shodan does a good job of respecting exclude lists and you can contact their
support team to request to be added to their ignore list.

### Vulnerable Infrastructure Search

[ICS Radar][icsradar]

[ICS Map][icsmap]

[Bacnet (port 47808)][baclearn]

[DNP3 (port 20000)][dnplearn]

[EtherNet/IP (port 44818)][ethlearn]

[Niagara Fox (ports 1911 and 4911)][niaglearn]

[IEC-104 (port 2404)][ieclearn]

[Red Lion (port 789)][lionlearn]

[Modbus (port 502)][modlearn]

[Siemens S7 (port 102)][s7learn] (PDF)

### Big Data

[Elastic Stack is vulnerable to ransom ware][elasticransom].

[The NFL Elastic Stack was breached][elasticnfl].

## How to build a Shodan alternative

[Masscan][masscan]

[Elastic Stack][elastic]

```
$ masscan 0.0.0.0/0 -p80,21,22,23,25,110,143,443,3389,U:161 --banners --rate 1500000 -oB ccc-shodan.scan --exclude 224.0.0.0/4
```

## Answers

1. Shodan is a search engine for internet connected devices. It provides
   information such as ports, banners, and is an excellent source of
   intelligence on the current state of the internet.

2. A simple search for open ports, devices, or program names can be conducted
   directly from the Shodan web page in a manner similar to how Google,
   DuckDuckGo, or other search engines function.

3. You can build your own version of Shodan using tools like Masscan in
   combination with Elastic Search.

4. Shodan is useful when you are trying to perform reconnaissance. 

## Conclusion

Shodan is an excellent tool for searching the internet when you do not have the
capability to perform the search yourself. Using Shodan frees you from the
infrastructure management and liability that could come from running Masscan
from a server you own. Shodan is a simple to use tool that protects the user
from reprisal but also poses security dilemmas itself. Shodan requires an
account for some searching, logs users, and is also able to study information
such as what is being searched, clicked on, or viewed. You should not rely on
Shodan for any reason in which you could possibly be met with repirsal as it is
an opaque box.

Ultimately Shodan is a beneficial tool that allows you to conduct deep and
telling reconnaissance of a target all while never firing off a packet
yourself.

## Final Recommendations

1. Use Linux.

2. Learn tools but also learn how those tools work.

3. Practice.

4. Document.

[shodan]: https://www.shodan.io/ 'Search Engine For Things'
[masscan]: https://github.com/robertdavidgraham/masscan 'Masscan'
[elastic]: https://www.elastic.co/ 'Elastic Stack'
[icsradar]: https://ics-radar.shodan.io/ 'The ICS Radar'
[icsmap]: https://icsmap.shodan.io/ 'The ICS Map'
[dnplearn]: https://www.dnp.org/Pages/AboutDefault.aspx 'DNP Overview'
[baclearn]: http://www.bacnet.org/ 'BACnet Overview'
[ethlearn]: https://www.odva.org/Technology-Standards/EtherNet-IP/Overview 'ODVA Overview'
[niaglearn]: https://www.tridium.com/products-services/niagara4 'Niagara Overview'
[ieclearn]: https://www.psinentec.de/en/products/iec-104securityproxy/ 'IEC104 Overview'
[lionlearn]: http://www.redlion.net/ 'Product Page'
[modlearn]: https://www.rtaautomation.com/technologies/modbus-rtu/ 'ModBus Overview'
[s7learn]: https://cache.industry.siemens.com/dl/files/056/18652056/att_70829/v1/S7prv54_e.pdf 'S7 PDF'
[cfaa]: http://archive.is/7mbJM 'Computer Fraud and Abuse Act'
[selective]: https://en.wikipedia.org/wiki/Selective_enforcement 'Selective Enforcement'
[honeypots]: https://github.com/paralax/awesome-honeypots 'Best Honey Pots'
[elasticransom]: http://archive.is/JB9HM 'Ransomware for Elastic Stack'
[elasticnfl]: http://archive.is/qF0ez 'Elastic NFL'
[hostgif]: /../assets/images/gifs/07192018-introduction-to-shodan/host.gif 'Animated Display Of Host Command'
[shodansearchpic]: /../assets/images/inserts/07192018-shodan/shodan-example-search-1.png 'The Shodan Search Page'
[excludescan]: https://github.com/rjmolesa/masscan-exclude/blob/master/exclude.conf 'Masscan Exclude List'
