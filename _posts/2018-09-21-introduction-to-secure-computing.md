---
layout: post
title: "Introduction To Secure Computing"
date: 2018-09-19T04:52:43-07:00
author: RetroMe
summary: >
  Fear, Uncertainty, Doubt, and Disinformation exists around the modern computer
  processor. Revelations brought about from leaks has left the technology
  community concerned about what is being integrated into their hardware and
  what kind of vulnerabilities exist. This discussion will introduce concepts
  such as Active Management Technology, Trusted Execution Environments, and
  Out-Of-Band system access.
categories: computers
thumbnail: fa-desktop
tags:
 - intel
 - amd
 - amt
 - tee
 - phoenix linux users group
---

## Performance Objective

At the conclusion of the course the student will be able to:

1. Identify what a Trusted Execution Environment Is.

2. Explain what Out-Of-Band system access is.

3. Identify what Intel Active Management Technology Is.

4. Describe one vulnerability related to remote management tools.

5. Describe how active management technology is bad for our freedom.

## Introduction

Out-Of-Band management is an integral part of managing a data center, providing
24/7 support, and ensuring high availability when managing servers. The ability
to manage a server, troubleshoot, provide updates, and conduct other
maintenance from a remote location when the system is otherwise disabled is
invaluable in a business environment. Tools like Intel Active Management
Technology provide added value for businesses and enterprise level users who
require extremely high levels of remote access to their systems which they gain
by bartering in trust with their vendor of choice.

Some [imageboards][gbotnet] have popularized the phrase 'botnet' when referring
to any technology that advertises, data mines, or conducts surveillance on the
user with or without their permission. The issue with much of the technology
related to out-of-band management is that it is a black box and closed off from
inspection by the user. There is however an open source API called
[redfish][redfishapi] that provides a standard API for much of the out-of-band
management technology on the market.

## Common Activities Facilitated By Out-Of-Band Management Tools

Out-Of-Band management involves the use of dedicated channels for
managing network enabled devices. This can include dedicated hardware,
software, and even on-board miniature computers complete with CPU and OS that
work outside the bounds of the device itself to provide many different features
and tool sets.

1. Reboot/Power Cycle
2. Change Boot Order/Device
3. Set power thresholds
4. Serial console access via SSH
5. Event notification
6. Logging
7. Identify systems
8. Check temperature, fans, general health state, as well as power consumption
9. Check hard drive status or fault state

Examples of in-band management tools include VNC, SSH, serial ports, or remote
desktop tools.

## Intel Active Management Technology

[Intel has created a vulnerable management tool][cve2017intel] that has the
[stated purpose][intelamtintent] of enabling IT managers to discover, repair,
and help protect networked computing assets.

You can [learn][amtsetup] how these tools are activated by the user and setup
through the training material provided by radmin.

## vPro

[vPro][vprowiki] is the umbrella term used to describe a host of features in
which Intel AMT is ONE of those features. You can only get AMT with a vPro
chip, but vPro chips offer other features.

vPro processors are multi-core and provide Intel AMT, remote configuration,
Intel TXT, wired and wireless network connections, support for different
network defense protocols, and a host of virtualization technology tools.

## Intel AMT And Freedom

[The Free Software Foundation][fsfamt] states -

	Intel's Active Management Technology (AMT) is a proprietary remote management
	and control system for personal computers with Intel CPUs. It is dangerous
	because it has full access to personal computer hardware at a very low level,
	and its code is secret and proprietary.

## AMT and Shodan

[The Intel Active Management Engine][shodanamt] currently reports 4,437 results
as a search result on the Shodan search engine as of the creation of this talk.

### Exploits And More

1. [CVE-2017-5689][cve5689git] - A proof of concept for the intel management
	engine exploit
2. [AMT honey pot][amthoneypot]
3. Intel Management Engine [JTAG][jtaggit] Proof Of Concept

Can't we disable the ME using a python script though? Not on modern hardware,
no. The removal of the ME causes most computers to become bricked or otherwise
unusable. Some users report their systems reboot or lock every thirty minutes.

## Other Out-Of-Band Access Tools

1. IPMI
2. MCTP
3. Desktop and Mobile Architecture for System Hardware
4. HP Guardian Service Processor
5. HP Integrated Lights-Out (iLO)
6. Dell DRAC (iDRAC)
7. IBM Remote Supervisor Adapter
8. Management Interface M1000e (Dell)

## Wireless AMT and 3G

Is there a [secret mobile 3G connection][secret3g] in vPro chips? No. There is
[no secret][IntelAntiTheftPDF] Intel 3G connection for their processors. There
is an [overt method][IntelAntiTheftGuidePDF] by which you can remotely connect
or provide an 'encrypted' SMS to activate the computer remotely.

We know that [ISP][InternetFraud] companies are willing to defraud the
government, but is it also possible that they receive a pass on their behavior
because of their integral support in providing spy services on the globe? 

The Journal of Political Sciences & Public Affairs [reports][politicaljournal] - 

	At some point prior to June 5, 2013, the Department of Justice applied for and
	received a warrant authorizing the FISC to order Verizon Business Network
	Services to turn over on “an ongoing daily basis” phone call details including
	whom calls are placed to and from, when the calls were made, and how long they
	lasted. This is known as metadata. It contains the pertinent information
	relating to the phone call, except for the content of the call itself.

Spying is a full time job and these companies would need to 
[inject themselves with capital][spendingintel] in a manner that provides them 
with plausible deniability. This spending is known as [Black Budget][blackbudget]
expenditures. I believe the current affairs in relation to the missing $400
billion in public money ear marked for improvements did provide improvements.
It improved the business process of spying and enriched companies while
sacrificing the American publics ability to compete on a global scale.

## What is Minix?

[Minix][minixos] is technically the worlds most popular operating system with
copies installed and in use on nearly every Intel computer built since 2008.
Minix powers a full networking stack, file system, drivers, and a web server
available on nearly every computer. Minix generally runs in ring -3 (ring
Negative Three) and is almost never touched by the user of the computer. Your
kernal runs at ring 0 and most applications will run at ring 3. This means that
Minix and the Intel ME has total and undisputed control over your computer. 

Minix is a posix compliant and Unix like operating system that uses a
microkernel architecture.

A semi-sensationalist article is available from [Network World][minixworld].

## Is Intel Trusted Execution Technology Also AMT?

No. You will find AMT and TXT on the same chip because they are provided as
part of the package known as vPRO but they are not the same thing. The Intel
Trusted Execution technology is a method by which developers can use computing
power provided by a separate OS from their currently in use OS to protect data
and tools. AMT is for actively managing the hardware and OS using a separate OS
and tool set to do so.

## What is Intel TXT

A [primer][inteltxtprimer] is available from Intel on their trusted execution
technology. I provide an excerpt below.

	Intel® Trusted Execution Technology is a component of Intel® vPro™ processor
	technology, a set of innovative technologies from Intel that provide
	next-generation manageability and security for the business PC. Other key
	features of this platform include Intel® Active Management Technology and
	Intel® Virtualization Technology.

	To make sure that code is executing in this protected environment, attestation
	mechanisms verify that the system has correctly invoked Intel TXT. These
	capabilities complement other key features of Intel vPro processor technology,
	including Intel AMT and Intel VT. Intel AMT enhances the security and central
	remote management of business PCs by providing a firmware-based out-of band
	communication channel through which a management console can reach the PC even
	when it is powered off or the operating system is non-functional or missing.

	The primary goal of Intel TXT is to provide the ability for software to define
	a safe, isolated execution space within the larger system.

	Intel TXT provides mechanisms that can be used to establish a system as
	trusted.

	Many usage models associated with Intel TXT involve the use of an Intel
	VT-enabled VMM. VMMs provide isolation for OSs and applications that will make
	use of Intel TXT. That scenario allows for running a number of protected
	partitions, each in its own virtual machine.

	It should be noted that Intel TXT can launch an environment other than a VMM.
	This section captures some noteworthy considerations associated with the use of
	Intel TXT with and without a VMM:.If no VMM is present acting to isolate the OS
	and applications that will use Intel TXT, the environment must address that
	isolation issue using software solutions.

	As virtualization continues to become a mainstream technology that is more
	widely deployed by businesses of all sizes, software solutions that can take
	advantage of it to provide a trusted execution environment using Intel TXT
	stand to gain a competitive advantage in their market segments as they
	differentiate themselves from their competitors.

## Do buzzwords like trusted and secure mean it is secure?

[No.][intelvulns]

The [Trusted Execution Technology][inteltxtvuln] is not perfect.

Complication breeds [vulnerability][inteltxtvuln2].

## The Future Forecast

Digital serfdom is our future. The definition of a serf is a person who labors
or works on land owned by a lord. While a serf is not a slave they are
obligated to exchange labour for survival using the tools provided by a lord.
We will no longer own our devices or our data and will exist in segregated and
balkanized corners of the internet provided by company lords who rent us the
tools and technology we will use to enforce our own fealty.

Imagine you 'purchase' a computer, bring it home, and set it up. The box you
keep in your home provides basic access to a remote data center and all of your
data is stored in the cloud. The system you keep in your home is monitored and
controlled remotely and every action you take is verified for acceptability
within what ever parameters are pertinent at the time. Once the computer is
plugged in, someone else will be able to turn it on or off, access network
capabilities, and activate microphones or cameras at their discretion without
you having any say in the matter.

## Answers

1. Trusted Execution Environment is a secure area of a main processor that
   guarantees code and data loaded inside is confidential and integral.

2. Out-Of-Band management or system access is a dedicated management channel
   for device maintenance that allows a system admin to monitor and manage
   servers by remote control even when those machines are powered off or
   otherwise nonfunctional.

3. Intel Active Management Technology is a proprietary tool produced by Intel
   to provide Out-Of-Band management through the deployment of a separate
   microprocessor not normally exposed to the user. This allows the monitoring,
   maintenance, update, upgrade, and repair of a system from a remote location
   even when the OS is otherwise not accessible.

4. Critical firmware vulnerabilities exist in Intel AMT, Intel Standard
   Manageability, and Intel Small Business Technology enabled devices.
   Attackers have the ability to remotely gain access to PCs or devices that
   use these technologies.

5. Intel Active Management Technology is a proprietary remote control system
   baked into Intel CPUs that provides full access to the system at a sub Ring
   0 level. This technology has no public oversight and is not free software.

## Conclusion

What do we do if we are concerned by the current situation in relation to
technology and our future? We can try a [new method of communication][altnetwork]
or we can work towards regaining our confidence in technology through better
understanding of what the tools are and how they are being deployed.

We now build computers that require computers to work in tandem in order to
provide basic operation. Our devices require experts to describe their use and
are almost impossible to use at a level that is not far abstracted from the
hardware itself. The [C64][c64books] had books that described the inner
workings of the hardware, provided assembler training aimed at kids, and
revealed even the most hardware level operations to the user. This no longer
exists.

We could put mankind into space on [1960's level technology][apollosite] with a
[code base][apollogithub] that uses only the most rudimentary commands but we
now require multi gigahertz systems with trusted computing modules and
processors within processors attached to wireless communication devices in
order to facilitate shit posting and uploading images of our sexual organs to
dating sites.

The Intel Management Engine is not targeted at the home market and does not
provide a benefit that could not be better emulated with something more open
source and removed from the baked in mess that is our motherboards. If you need
remote management, system monitoring, and all of the tools provided by vPro,
you should be allowed to purchase the tool sets necessary and implement them as
you see fit. For those of us who do not need these tools, they should not be
sneaking quietly into our systems and left in as a backdoor. Also, for those
thinking to themselves they will simple [switch to AMD][amdinside] I urge you
to do your research. These systems are being baked into every computer, but the
World Governments continue to [call for more][vergebackdoors].

### The Five Eyes Brief From The AU Government

Read the [Five Eyes Brief][aufiveyesreport] online thanks to the Australian
Government.

## Final Recommendations

1. Use Linux.
2. Understand your hardware.
3. Understand your OS.
4. Read.
5. Learn to search.

[cve2017intel]: http://archive.is/DyzHX 'Intel AMT Vulnerability'
[intelamtintent]: http://archive.is/7bz5L 'The AMT Intel Site'
[fsfamt]: http://archive.is/xHke5 'The FSF statement on Intel AMT'
[amtsetup]: http://archive.is/zceWM 'How to setup Intel AMT'
[shodanamt]: https://www.shodan.io/search?query=http+intel+active+management 'Current IAMT Boxes'
[gbotnet]: http://archive.is/dPF0B 'It is botnet'
[redfishapi]: https://www.dmtf.org/standards/redfish 'The redfish standards'
[secret3g]: http://archive.is/MAdxo 'Secret 3G connections in Intel Chips?'
[IntelAntiTheftPDF]: /../assets/pdf/intel_antitheft.pdf 'Intel Anti Theft Services PDF'
[IntelAntiTheftGuidePDF]: /../assets/pdf/intel_antitheft_userguide.pdf 'Intel Anti Theft User Guide PDF'
[InternetFraud]: http://archive.is/fN9L3 'Broadband Companies Robbing us?'
[politicaljournal]: http://archive.is/ryouW 'The Intelligence Club: A Comparative Look at Five Eyes'
[spendingintel]: http://archive.is/7FNNs 'Only the Pentagon could spend $640 on a toilet seat'
[blackbudget]: https://en.wikipedia.org/wiki/Black_budget 'What is black budget'
[altnetwork]: https://tools.ietf.org/html/rfc2549 'How do we replace the network?'
[inteltxtprimer]: http://archive.is/0AoP4 'The Intel TXT Primer'
[intelvulns]: https://www.cvedetails.com/vulnerability-list/vendor_id-238/Intel.html 'The CVE list for Intel'
[inteltxtvuln]: https://www.cvedetails.com/cve/CVE-2011-5174/ 'A TXT vulnerability'
[inteltxtvuln2]: https://www.cvedetails.com/cve/CVE-2013-5740/ 'A TXT vulnerability'
[c64books]: https://archive.org/details/commodore_c64_books 'A collection of books on the C64 including Basic for Kids'
[apollosite]: http://archive.is/wc6Z0 'The computers of apollo 11'
[apollogithub]: https://github.com/chrislgarry/Apollo-11/ 'The Apollo 11 code base'
[minixos]: https://www.minix3.org/ 'The Minix OS'
[minixworld]: http://archive.is/U59Va 'Minix Network World article'
[vprowiki]: https://en.wikipedia.org/wiki/Intel_vPro 'vPro on WIKI'
[cve5689git]: https://github.com/qazbnm456/awesome-cve-poc/blob/master/CVE-2017-5689.md 'Proof Of Concept For ME Exploit'
[amthoneypot]: https://github.com/packetflare/amthoneypot 'A honey pot for the intel me'
[jtaggit]: https://github.com/ptresearch/IntelTXE-PoC 'The JTAG for AMT'
[amdinside]: http://archive.is/Gz0JL 'AMD is also implementing similar tools to Intel'
[vergebackdoors]: https://webcache.googleusercontent.com/search?q=cache:https://www.theverge.com/2018/9/3/17815196/five-eyes-encryption-backdoors-us-uk-australia-nz-canada 'Five Eyes Wants More Backdoors'
[aufiveyesreport]: http://archive.is/Tk44X 'The Five Eyes Report Provided By Australia'
