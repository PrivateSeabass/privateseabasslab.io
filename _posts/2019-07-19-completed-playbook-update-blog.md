---
layout: post
title: "19 Jul 2019: Completed Playbooks & Updates"
date: 2019-07-19T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Quite a few things have happened, as it has been a while.
  Playbooks for automatic device setup is done.
  plans for the future.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
 - security
 - VPN
 - IPv6
---
[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[SSA]: https://silentsysadmin.com/pages/About.html 'The Silent Systems Administrator'

For anyone who reads my other posts, I was setting the playbooks up for my graduation project in college. 
I have completed the project to the extent that I can automate pretty much any client and setup the basics for any uplinks.
An ansible role would probably need to be created for mesh uplinks to properly set it up on mass, but this works for a single device.
If they aren't there currently, there are stars in the documentation designating what needs to be changed to allow for another DHCP server on the network. 
For researcers, you may only need one mesh device to observer what you want. 
With any networking experience, it should be easy for people to change.  

That being said, [Project Phoenix][ProjectPhoenix] is looking into IPv6 and using that instead of IPv4.  
Considerations:
* Removal of DHCP
* ICMPv6
* NAT vs not

#### Removal of DHCP
The fact that DHCP is used means that there is some form of centralization in the network. 
This is a problem for our project, as it means we can't let the community totally own it, instead of having to manage who owns a DHCP server (and thus an IP).
We have no intention of becoming a non-profit WISP (Wireless ISP).
Not our objective, it would reduce education focus, it would restrict rather than enable people, and it is just too much work for us to care to manage.
¯\\\_(ツ)\_/¯  

IPv6 means that the devices are supposed to be able to configure themselves with an IP address.  
"How?" you ask? WELL...

#### ICMPv6
When a device connects to an IPv6 network, the device sends out an ICMPv6 to determine what other devices there are and what IPs are taken.
This is ideal for out mesh network, as it means we don't need DHCP servers to determine what the user's IP is and actually decentralizes the network.
Though there are problems we don't have an answer to yet, it is still the better option, as IPv4 is difficult to use for a scaling mesh network.   
Problems:
* IP spoofing
* DOS (mass amount of usable IPs for it)
* NAT
  * If we have two nodes, one with and one without NAT, does the user's ports show on the open internet?
  * Removal makes internet traffic faster, but security training will be more necessary (because open ports on the internet)
  * Used or not?

#### NAT vs not
A personal consideration is if the Project Phoenix mesh network should use NAT by default.
People wouldn't need to worry about installing programs which open ports.
Since we are doing this project for the purpose of teaching the populous, it may not be that bad.
The problem is that not everyone will seek computer education, and we still want to be accessible to them.  
In reality, if they are learning about the system they may setup an uplink node, and they could modify it to not use NAT.
Since we have mesh, and the internet could be accessed though the just created route, which we don't manage... meaning, purpose for NAT may be voided.

## Future Vision
For my part, since automating these devices is complete, I need to decide the next focus for what I'll be developing for [Project Phoenix][ProjectPhoenix].
I've already fixed the client section of [Projec Mystery Castle][PMC], [Will][Lead] said he would fix the documentation for other portions, and so I'll be starting on researching how to implement Wireguard onto our mesh.  

As a result, I'll be bringing in a friend of mine and we will work together on this.
Benefit for him is being able to use this for his graduation project.  
Otherwise, [Lead-Will][Lead] be continuing the developement of hardware for our demo.
As I recall, mostly work with OSPF, IPv6, and hardware.
Wanna ask him about it? His [site][SSA] is up! Ask him about it!

We have dates planned for a showcase and a fund raiser in the future.  
As it stands, time for new research to begin!  
Onward!
