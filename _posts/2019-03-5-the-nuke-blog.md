---
layout: post
title: "5 Mar. 2019: The Nuke"
date: 2019-03-5T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  New story, this time on the effect of a nuke.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - mesh
 - networking
 - nuke
---
[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[PB]: https://piratebox.cc/ 'Pirate Box'
[Relay]: https://www.eff.org/torchallenge/what-is-tor.html 'Tor Relays'
[batWrt]: https://www.open-mesh.org/projects/batman-adv/wiki/Batman-adv-openwrt-config 'batman-adv for OpenWrt'
[openWrt]: https://openwrt.org/ 'openwrt site'
[ubik]: https://store.ui.com/products/nanostation-m5 'Ubiquity Hardware'

### What happens to the internet, when a nuke gets dropped?

Alright, forget reliability, I just wanted to share this, because it described it in surprising detail.
[Fallout manual containing the process of the atomic bomb's explosion](/assets/other/Fallout_manual_English.pdf)
(No, really, it goes over it on page 1-2)

Needless to say, the infrastructure would get destroyed.

### How to get internet there
Needless to say, the main companies and towers that would maintain internet connections would have been damaged, if not obliterated in the shockwave. 
There would need to be a method of delivering internet access to the users (such as the recovery effort) in the area.
Cell towers would be too far for mobile data and finding functional wires (let alone WiFi) is unlikely.  

One method could be taking a radio tower and hoping it reaches far enough. 
One could set up a wireless internet dish, but it is rather directed. 
This means our devices need to have a large circular radius or rotate across the irradiated area, as the person moves (also assuming it reaches).  
What if we used [Mesh networking][BATMAN]?

Chances are, the previously mentioned devices will still want to be used.
But now, users are able to bounce off each other to reach the access point that would have been out of range. 
Because of the mesh, users could either extend each other's range, or they could bring additional batter powered access points to communicate back to where there is internet connectivity.

Doing more hops through devices result in slower internet, but since it is still possible, one could line up battery powered access points to connect a user to the area where there is internet access. 
Less likely, but since technology gets better over time, I found it worth mentioning.
