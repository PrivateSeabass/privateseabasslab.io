---
layout: post
title: "3 Jun 2019: Status and Updates for the Future"
date: 2019-06-3T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  How the current setup for the playbook is and what possible paths discussed for the future are.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
 - client
 - playbook
 - ansible
---
[kolibri]: https://learningequality.org/kolibri/ 'Offline education'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[debNet]: https://wiki.debian.org/NetworkingConfigurations 'Debian Network config page'
[willSite]: https://silentsysadmin.com/ 'Website of Will McDonald'
[PCA]: https://www.phoenixunion.org/coding 'Phoenix Coding Academy'

## First Update
The Ansible playbook for clients is functional.
Users are able to use the playbook to automatically configure devices to automatically be configured on start-up as long as the Linux distribution has networking.service being run by default.
This is all well and good, especially when a page like the [DEBIAN NETWORK CONFIGURATION PAGE][debNet] describes using networkmanager.service for simpler automatic configs and networking.service for anything that needs more in depth configs.  
Eventually, I figured out that the service is added by the package ifupdown.
As a result, until I learn how to implement this in Network Manager or Netbag (could be off on the name), I will also be ensuring the ifupdown package exists.  
I believe there is an ifupdown version 2, so of course, that one is preferable, but I also believe ifupdown is deprecating.
Once all of these automation scripts are complete, I may look into changing to Network Manager.  

This being said, all common Debian based devices _should_ actually function with the client playbook, now.


## Fresh Idea
There are three methods we have in mind for how mesh could progress into popularity.  

The first is least likely to occur for [our group][ProjectPhoenix].  
We could do a case study, in which we go to a community far away enough, telcos do not care to give internet.
There the government could do tests for reliability and determine if they want to use it.  

Second is what we were going for.  
Find a community telcos do not care about, and provide them with tools to set up a mesh network. 
Get businesses to supply the internet, and provide the community with devices and support.
Use non-profit resources and gain political support through this being a humanitarian effort.
Hope telcos don't mind.

The newest thought was the same as the previous except the last sentence and including as follows:  
The area is known to be under served, because it isn't worth maintaining the infrastructure to provide the area with internet.
If we learned where communities are under served, we can add a mesh, and still provide the telcos with money, because we are paying them to supply more internet to a super-node.
This means that the community gets a mesh network and \*insert local telco name here\* gets paid.
If we can work with the telcos, they can also tell us what communities they under serve, meaning where to go next.
More high profile, but it is otherwise a win-win.

##### Note
Turns out Verizon has an employee volunteering thing, which makes the third option desirable.

I should also mention that what we end up doing may change, but I plan to mention that in a new post if the change occurs.

## Stuff We Are Looking At
Currently, [Will][Lead], the lead developer, is looking into alpine Linux for router OS.  

We need to do some research on what "libre mesh" is. 
As of today, someone told me about it for router firmware and how it might be useful in our... shifty (unsolidified?)... infrastructure plans (but not for that reason).
I asked [Will][willSite] if he knows anything about it, but I haven't heard back yet.  

We are looking into adding [Kolibri][kolibri] for use, even when mesh is disconnected from the outer internet.  

[Phoenix Coding Academy][PCA] is supposedly making a library for us, so we could also host those resources somewhere, too.
I'll post more on it when I am more familiar with what that plan is.


That is all for this update, I should have more soon.
Things are speeding up.
